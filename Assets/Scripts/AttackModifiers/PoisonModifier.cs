﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonModifier : AttackModifier
{

    public int PoisonDuration = 2;
    public int PoisonDamage = 1;

    // Use this for initialization
    public PoisonModifier(string typeName, string displayName, int frequency, int poisonDuration, int poisonDamage) : base(typeName, displayName, frequency)
    {
        PoisonDuration = poisonDuration;
        PoisonDamage = poisonDamage;
        Debug.Log("PoisonModifier created!");
    }

    public override bool ApplyEffect(LivingEntity target, int damage, List<AttackModifier> modifiers)
    {
        Debug.Assert(Frequency != 0, "Frequency on poison mod is zero!");
        if (EffectProcs())
        {
            target.Poison(PoisonDuration, PoisonDamage);
        }
        return base.ApplyEffect(target, damage, modifiers);
    }
}

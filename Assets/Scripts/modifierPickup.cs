﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class modifierPickup : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Pickup()
    {
        GameManager.RemovePickup(this);
        Destroy(gameObject);
    }
}

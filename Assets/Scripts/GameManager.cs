﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    // Balance constants
    private const int attackModOptions = 2;
    public static List<AttackModifier> AvailableAttackModifiers = new List<AttackModifier>
    {
        new CritModifier("crit", "Critical Hit", 4, 2),
        new SplashModifier("splash", "Sweeping Strike", 3),
        new StunModifier("stun", "Stunning Blow", 5),
        new PoisonModifier("poison", "Envenomed Blade", 2, 4, 1),
    };
    public const float gridSize = 0.7f;
    // These next four should be a multiple of gridSize
    private const float minXCoOrd = -9.8f;
    private const float maxXCoOrd = 9.8f;
    private const float minYCoOrd = -4.2f;
    private const float maxYCoOrd = 4.2f;
    private const int enemySpawnRate = 15;
    private static int enemySpawnCount = 1; // Remember that this is increased on the zeroth turn
    private const int enemySpawnRateIncreaseRate = 4 * enemySpawnRate;
    private const int enemySpawnRateIncrease = 1;
    private const int pickupSpawnRate = 100;
    private const int killsPerPickup = 2;

    // Stat trackers
    private static int turnNumber;
    private static int killCount;

    // Things that should not be set manually
    private static List<enemy> enemies;
    public static Grid WorldGrid;
    private static player Player;
    public static GameObject mainCanvas;
    private static Text turnDisplay;
    private static Text killCounter;
    private static Text currentModifiers;
    private static LivingEntity[][] entitiesGrid;
    private static modifierPickup[][] pickupsGrid;
    private static int arrayOffsetX;
    private static int arrayOffsetY;
    private static List<AttackModifier> selectableAttackModifiers = new List<AttackModifier>();
    public static Vector3Int maxArrayPosition;


    // Use this for initialization
    void Start()
    {
        turnNumber = 0;
        killCount = 0;
        enemies = new List<enemy>();

        WorldGrid = this.GetComponent<Grid>();
        WorldGrid.cellSize = new Vector3(gridSize, gridSize, 0f);

        Player = FindObjectOfType<player>();
        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        turnDisplay = GameObject.FindGameObjectWithTag("TurnDisplay").GetComponent<Text>();
        killCounter = GameObject.FindGameObjectWithTag("KillCounter").GetComponent<Text>();
        currentModifiers = GameObject.FindGameObjectWithTag("CurrentModifiers").GetComponent<Text>();

        Vector3Int gridMaxCell = WorldGrid.WorldToCell(new Vector3(maxXCoOrd, maxYCoOrd, 0f));
        Vector3Int gridMinCell = WorldGrid.WorldToCell(new Vector3(minXCoOrd, minYCoOrd, 0f));
        int gridWidth = gridMaxCell.x - gridMinCell.x + 1;
        int gridHeight = gridMaxCell.y - gridMinCell.y + 1;
        entitiesGrid = new LivingEntity[gridWidth][];
        for (int i = 0; i < entitiesGrid.Length; i++)
        {
            entitiesGrid[i] = new LivingEntity[gridHeight];
            for (int j = 0; j < entitiesGrid[i].Length; j++)
            {
                entitiesGrid[i][j] = null;
            }
        }

        pickupsGrid = new modifierPickup[gridWidth][];
        for (int i = 0; i < pickupsGrid.Length; i++)
        {
            pickupsGrid[i] = new modifierPickup[gridHeight];
            for (int j = 0; j < pickupsGrid[i].Length; j++)
            {
                pickupsGrid[i][j] = null;
            }
        }
        arrayOffsetX = (gridMaxCell.x - gridMinCell.x) / 2;
        arrayOffsetY = (gridMaxCell.y - gridMinCell.y) / 2;

        Vector3Int playerArrayPosition = WorldToArray(Player.transform.position);
        entitiesGrid[playerArrayPosition.x][playerArrayPosition.y] = Player;

        maxArrayPosition = WorldToArray(new Vector3(maxXCoOrd, maxYCoOrd, 0f));

        CreateBackGround();
    }

    // Update is called once per frame
    void Update()
    {
        turnDisplay.text = "Turn: " + turnNumber;
        killCounter.text = "Enemies Defeated: " + killCount;
    }

    private void CreateBackGround()
    {
        GameObject background = GameObject.Find("Background");
        Vector3Int minGridPosition = WorldGrid.WorldToCell(new Vector3(minXCoOrd, minYCoOrd, 0f));
        Vector3Int maxGridPosition = WorldGrid.WorldToCell(new Vector3(maxXCoOrd, maxYCoOrd, 0f));
        for (int i = minGridPosition.x - 2; i <= maxGridPosition.x + 2; i++)
        {
            for (int j = minGridPosition.y - 2; j <= maxGridPosition.y + 2; j++)
            {
                GameObject newBackGroundTile = Instantiate(Resources.Load("Prefabs/backGroundTile", typeof(GameObject))) as GameObject;
                newBackGroundTile.transform.SetParent(background.transform);
                newBackGroundTile.transform.position = WorldGrid.CellToWorld(new Vector3Int(i, j, 0));
            }
        }
    }

    ///////////////////////// GENERAL GETTERS /////////////////////////
    public static int GetTurnNumber()
    {
        return turnNumber;
    }

    public static player GetPlayer()
    {
        return Player;
    }

    ///////////////////////// END TURN /////////////////////////
    public static void EndTurn()
    {
        foreach (enemy CurrentEnemy in enemies)
        {
            CurrentEnemy.TakeTurn();
        }

        if (turnNumber % enemySpawnRateIncreaseRate == 0)
        {
            enemySpawnCount += enemySpawnRateIncrease;
        }

        if (turnNumber % enemySpawnRate == 0)
        {
            for (int i = 0; i < enemySpawnCount; i++)
            {
                SpawnEnemy(NewEmptyPosition());
            }
        }

        if (turnNumber % pickupSpawnRate == 0)
        {
            SpawnPickup(NewEmptyPosition());
        }

        turnNumber++;
    }

    ///////////////////////// GRID UTILITIES /////////////////////////
    public static Vector3 SnapToGrid(Vector3 position)
    {
        return WorldGrid.CellToWorld(WorldGrid.WorldToCell(position));
    }

    public static Vector3Int WorldToArray(Vector3 position)
    {
        Vector3Int arrayPosition = WorldGrid.WorldToCell(position);
        arrayPosition.x += arrayOffsetX;
        arrayPosition.y += arrayOffsetY;

        return arrayPosition;
    }

    public static bool SpaceIsInBounds(Vector3 position)
    {
        Vector3Int arrayPosition = WorldToArray(position);
        return SpaceIsInBounds(arrayPosition);
    }

    public static bool SpaceIsInBounds(Vector3Int arrayPosition)
    {
        if (arrayPosition.x < 0 || arrayPosition.x >= entitiesGrid.Length || arrayPosition.y < 0 || arrayPosition.y >= entitiesGrid[0].Length)
        {
            return false;
        }
        return true;
    }

    public static bool SpaceIsOccupied(Vector3 position)
    {
        Vector3Int arrayPosition = WorldToArray(position);

        return entitiesGrid[arrayPosition.x][arrayPosition.y] != null;
    }

    public static bool SpaceContainsPickup(Vector3 position)
    {
        Vector3Int arrayPosition = WorldToArray(position);

        return pickupsGrid[arrayPosition.x][arrayPosition.y] != null;
    }

    public static LivingEntity GetEntityAt(Vector3 position)
    {
        return GetEntityAt(WorldToArray(position));
    }

    public static LivingEntity GetEntityAt(Vector3Int arrayPosition)
    {
        if (arrayPosition.x < 0 || arrayPosition.y < 0 || arrayPosition.x > maxArrayPosition.x || arrayPosition.y > maxArrayPosition.y)
        {
            return null;
        }
        return entitiesGrid[arrayPosition.x][arrayPosition.y];
    }

    private static Vector3 NewEmptyPosition()
    {
        Vector3 position;
        do
        {
            position = new Vector3(Random.Range(minXCoOrd, maxXCoOrd), Random.Range(minYCoOrd, maxYCoOrd));
        } while (SpaceIsOccupied(position) || SpaceContainsPickup(position));
        return position;
    }

    ///////////////////////// GRID MODIFIERS /////////////////////////
    public static bool MoveEntity(LivingEntity entity, Vector3 oldPosition, Vector3 newPosition)
    {
        Vector3Int oldArrayPosition = WorldToArray(oldPosition);
        Vector3Int newArryPosition = WorldToArray(newPosition);

        if (newArryPosition.x < 0 || newArryPosition.x >= entitiesGrid.Length || newArryPosition.y < 0 || newArryPosition.y >= entitiesGrid.Length)
        {
            return false;
        }

        // Verify that we aren't moving something else
        if (entity != entitiesGrid[oldArrayPosition.x][oldArrayPosition.y])
        {
            return false;
        }

        entitiesGrid[oldArrayPosition.x][oldArrayPosition.y] = null;
        entitiesGrid[newArryPosition.x][newArryPosition.y] = entity;

        return true;
    }

    public static void RemoveEntity(LivingEntity entity)
    {
        Vector3Int arrayPosition = WorldToArray(entity.transform.position);
        if (entitiesGrid[arrayPosition.x][arrayPosition.y] == entity)
        {
            entitiesGrid[arrayPosition.x][arrayPosition.y] = null;
        }
    }

    public static void RemoveEnemy(enemy Enemy)
    {
        enemies.Remove(Enemy);
        killCount++;
        if (killCount % killsPerPickup == 0)
        {
            SpawnPickup(NewEmptyPosition());
        }
        GetPlayer().GotKill();
    }

    public static void RemovePickup(modifierPickup pickup)
    {
        Vector3Int arrayPosition = WorldToArray(pickup.transform.position);
        if (pickupsGrid[arrayPosition.x][arrayPosition.y] == pickup)
        {
            pickupsGrid[arrayPosition.x][arrayPosition.y] = null;
        }
    }

    private static GameObject SpawnEnemy(Vector3 position)
    {
        // Instantiate new enemy.
        GameObject newEnemy = Instantiate(Resources.Load("Prefabs/enemy", typeof(GameObject))) as GameObject;
        // Position new enemy.
        position = SnapToGrid(position);
        newEnemy.transform.position = position;
        // Add new enemy to list of enenmies and entitiesGrid
        enemy newEnemyScript = newEnemy.GetComponent<enemy>();
        enemies.Add(newEnemyScript);
        Vector3Int arrayPosition = WorldToArray(position);
        entitiesGrid[arrayPosition.x][arrayPosition.y] = newEnemyScript;
        return newEnemy;
    }

    private static GameObject SpawnPickup(Vector3 position)
    {
        if (AvailableAttackModifiers.Count == 0)
        {
            return null;
        }
        // Instantiate new pickup.
        GameObject newPickup = Instantiate(Resources.Load("Prefabs/ModifierPickup", typeof(GameObject))) as GameObject;
        // Position new pickup.
        position = SnapToGrid(position);
        newPickup.transform.position = position;
        // Add new pickup to array of pickups.
        modifierPickup newPickupScript = newPickup.GetComponent<modifierPickup>();
        Vector3Int arrayPosition = WorldToArray(position);
        pickupsGrid[arrayPosition.x][arrayPosition.y] = newPickupScript;
        return newPickup;
    }

    public static void PickupModifier(Vector3 position)
    {
        Vector3Int arrayPosition = WorldToArray(position);
        if (AvailableAttackModifiers.Count == 0)
        {
            pickupsGrid[arrayPosition.x][arrayPosition.y].Pickup();
            return;
        }
        // Determine list of modifiers to pick from.
        for (int i = 0; i < attackModOptions && AvailableAttackModifiers.Count > 0; i++)
        {
            int randInt = Mathf.FloorToInt(Random.Range(0, AvailableAttackModifiers.Count));
            selectableAttackModifiers.Add(AvailableAttackModifiers[randInt]);
            AvailableAttackModifiers.RemoveAt(randInt);
        }
        // Display list of available mods.
        GameObject modifierPicker = Instantiate(Resources.Load("Prefabs/modifierPicker", typeof(GameObject))) as GameObject;
        modifierPicker.transform.SetParent(mainCanvas.transform, false);
        Text modifierPickerText = modifierPicker.GetComponent<Text>();
        for (int i = 0; i < selectableAttackModifiers.Count; i++)
        {
            modifierPickerText.text += "\n" + (i + 1) + ": " + selectableAttackModifiers[i].DisplayName;
            modifierPickerText.text += " - frequency: " + selectableAttackModifiers[i].Frequency;
        }

        pickupsGrid[arrayPosition.x][arrayPosition.y].Pickup();
        GetPlayer().selectingMod = true;
    }

    public static bool SelectModifier(int index)
    {
        // Verify the index is not out of bounds.
        if (index >= selectableAttackModifiers.Count)
        {
            return false;
        }

        // Add the new modifier.
        if (!GetPlayer().AddAttackModifier(selectableAttackModifiers[index]))
        {
            return false;
        }

        // Add the new modifier to the list of modifiers.
        currentModifiers.text += "\n" + selectableAttackModifiers[index].DisplayName + " - frequency: " + selectableAttackModifiers[index].Frequency;

        // Return the available attack mods to the list of mods the player doesn't have.
        selectableAttackModifiers.RemoveAt(index);
        foreach (AttackModifier attackMod in selectableAttackModifiers)
        {
            AvailableAttackModifiers.Add(attackMod);
        }
        selectableAttackModifiers = new List<AttackModifier>();

        // Destroy the modifierPickerText.
        Destroy(GameObject.FindGameObjectWithTag("ModifierPicker"));

        GetPlayer().selectingMod = false;

        return true;
    }
}

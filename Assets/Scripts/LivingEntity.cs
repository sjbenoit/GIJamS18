﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LivingEntity : MonoBehaviour
{
    // Balance constants
    [SerializeField]
    protected int maxHealth = 10;
    [SerializeField]
    public int damage = 1;

    // Stat trackers
    protected int health;
    protected int isStunned;
    protected int poisonDuration;
    protected int poisonDamage;

    // Private/protected object references
    protected Text healthDisplay;

    // Use this for initialization
    protected virtual void Start () {
        health = maxHealth;
        isStunned = 0;

        // Create and position healthDisplay
        GameObject mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        GameObject healthDisplayObject = Instantiate(Resources.Load("Prefabs/HealthDisplay", typeof(GameObject))) as GameObject;
        healthDisplayObject.transform.SetParent(mainCanvas.transform);
        healthDisplay = healthDisplayObject.GetComponent<Text>();
        Vector3 centeredPosition = transform.position + new Vector3(-0.25f, -0.175f, 0f);
        healthDisplay.transform.position = Camera.main.WorldToScreenPoint(centeredPosition);
    }
	
	// Update is called once per frame
	protected virtual void Update () {
        healthDisplay.text = health + "/" + maxHealth;
        Vector3 centeredPosition = transform.position + new Vector3(-0.25f, -0.175f, 0f);
        healthDisplay.transform.position = Camera.main.WorldToScreenPoint(centeredPosition);
    }

    protected virtual bool Attack(LivingEntity target)
    {
        // Attack a thing.
        return true;
    }

    protected virtual bool Move(Vector3 direction)
    {
        Vector3 normalizedDirection = direction.normalized;
        Vector3 oldPosition = transform.position;
        Vector3 newPosition = oldPosition + normalizedDirection * GameManager.gridSize;
        if (!GameManager.SpaceIsInBounds(newPosition))
        {
            return false;
        }
        if (GameManager.SpaceIsOccupied(newPosition))
        {
            return false;
        }
        transform.position = newPosition;
        if (normalizedDirection == Vector3.up)
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            healthDisplay.transform.localPosition = new Vector3(0f, -0.1f, -1f);
        }
        else if (normalizedDirection == Vector3.left)
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
            healthDisplay.transform.localPosition = new Vector3(-0.1f, 0f, -1f);
        }
        else if (normalizedDirection == Vector3.down)
        {
            transform.rotation = Quaternion.Euler(0, 0, 180);
            healthDisplay.transform.localPosition = new Vector3(0f, 0.1f, -1f);
        }
        else if (normalizedDirection == Vector3.right)
        {
            transform.rotation = Quaternion.Euler(0, 0, 270);
            healthDisplay.transform.localPosition = new Vector3(0.1f, 0f, -1f);
        }
        healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);

        return GameManager.MoveEntity(this, oldPosition, newPosition);
    }

    // returns true on kill
    public virtual bool TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            // Destroy this object.
            GameManager.RemoveEntity(this);
            Destroy(healthDisplay.gameObject);
            Destroy(gameObject);

            return true;
        }

        return false;
    }

    public virtual void Stun(int duration)
    {
        isStunned = duration;
    }

    public virtual void Poison(int duration, int damage)
    {
        poisonDuration = duration;
        poisonDamage = damage;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunModifier : AttackModifier
{

    public int stunDuration = 2;

    // Use this for initialization
    public StunModifier(string typeName, string displayName, int frequency):base(typeName, displayName, frequency)
    {
        Debug.Log("StunModifier created!");
    }

    public override bool ApplyEffect(LivingEntity target, int damage, List<AttackModifier> modifiers)
    {
        Debug.Assert(Frequency != 0, "Frequency on stun mod is zero!");
        if (EffectProcs())
        {
            target.Stun(stunDuration);
        }
        return base.ApplyEffect(target, damage, modifiers);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class player : LivingEntity
{
    // Balance constants
    [SerializeField]
    private int healPerKill;

    // Private variables that should not be manually set.
    private bool attacking;
    public bool selectingMod;
    private List<AttackModifier> sortedAttackMods;
    public Dictionary<string, AttackModifier> acquiredAttackMods;
    private Text statsDisplay;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();
        attacking = false;
        selectingMod = false;
        sortedAttackMods = new List<AttackModifier>();
        acquiredAttackMods = new Dictionary<string, AttackModifier>();

        statsDisplay = GameObject.Find("StatsDisplay").GetComponent<Text>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        UpdateStatsDisplay();

        bool actionSuccessful = false;
        // Controls
        if (selectingMod)
        {
            if (Input.GetKeyUp(KeyCode.Alpha1))
            {
                GameManager.SelectModifier(0);
            }
            else if (Input.GetKeyUp(KeyCode.Alpha2))
            {
                GameManager.SelectModifier(1);
            }
        }
        else if (Input.GetKeyUp(KeyCode.Q))
        {
            attacking = !attacking;
            Debug.Log("Attacking: " + attacking);
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            if (attacking)
            {
                // Attack
                Vector3Int targetArrayPosition = GameManager.WorldToArray(transform.position);
                targetArrayPosition.y += 1;
                LivingEntity target = GameManager.GetEntityAt(targetArrayPosition);
                actionSuccessful = Attack(target);
                if (actionSuccessful)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(0f, -0.1f, -1f);
                }
            }
            else
            {
                actionSuccessful = Move(Vector3.up);
            }
        }
        else if (Input.GetKeyUp(KeyCode.A))
        {
            if (attacking)
            {
                // Attack
                Vector3Int targetArrayPosition = GameManager.WorldToArray(transform.position);
                targetArrayPosition.x -= 1;
                LivingEntity target = GameManager.GetEntityAt(targetArrayPosition);
                actionSuccessful = Attack(target);
                if (actionSuccessful)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 90);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(-0.1f, 0f, -1f);
                }
            }
            else
            {
                actionSuccessful = Move(Vector3.left);
            }
        }
        else if (Input.GetKeyUp(KeyCode.S))
        {
            if (attacking)
            {
                // Attack
                Vector3Int targetArrayPosition = GameManager.WorldToArray(transform.position);
                targetArrayPosition.y -= 1;
                LivingEntity target = GameManager.GetEntityAt(targetArrayPosition);
                actionSuccessful = Attack(target);
                if (actionSuccessful)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 180);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(0f, 0.1f, -1f);
                }
            }
            else
            {
                actionSuccessful = Move(Vector3.down);
            }
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            if (attacking)
            {
                // Attack
                Vector3Int targetArrayPosition = GameManager.WorldToArray(transform.position);
                targetArrayPosition.x += 1;
                LivingEntity target = GameManager.GetEntityAt(targetArrayPosition);
                actionSuccessful = Attack(target);
                if (actionSuccessful)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 270);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(0.1f, 0f, -1f);
                }
            }
            else
            {
                actionSuccessful = Move(Vector3.right);
            }
        }

        if (actionSuccessful)
        {
            GameManager.EndTurn();
        }
    }

    private void UpdateStatsDisplay()
    {
        statsDisplay.text = "Damage: ";

        if (acquiredAttackMods.ContainsKey("crit"))
        {
            int critFrequency = acquiredAttackMods["crit"].Frequency;
            int turnsToCrit = (critFrequency - (GameManager.GetTurnNumber() % critFrequency)) % critFrequency;
            if (turnsToCrit == 0)
            {
                statsDisplay.text += damage * CritModifier.CritMultiplier;
                statsDisplay.text += "\nUpcoming " + acquiredAttackMods["crit"].DisplayName + " flourishes: now";
            }
            else
            {
                statsDisplay.text += damage;
                statsDisplay.text += "\nUpcoming " + acquiredAttackMods["crit"].DisplayName + " flourishes: " + turnsToCrit;
            }
            statsDisplay.text += ", " + (turnsToCrit + critFrequency) + ", " + (turnsToCrit + 2 * critFrequency);
        }
        else
        {
            statsDisplay.text += damage;
        }

        if (acquiredAttackMods.ContainsKey("splash"))
        {
            statsDisplay.text += "\nUpcoming " + acquiredAttackMods["splash"].DisplayName + " flourishes: ";
            int splashFrequency = acquiredAttackMods["splash"].Frequency;
            int turnsToSplash = (splashFrequency - (GameManager.GetTurnNumber() % splashFrequency)) % splashFrequency;
            if (turnsToSplash == 0)
            {
                statsDisplay.text += "now";
            }
            else
            {
                statsDisplay.text += turnsToSplash;
            }
            statsDisplay.text += ", " + (turnsToSplash + splashFrequency) + ", " + (turnsToSplash + 2 * splashFrequency);
        }

        if (acquiredAttackMods.ContainsKey("stun"))
        {
            statsDisplay.text += "\nUpcoming " + acquiredAttackMods["stun"].DisplayName + " flourishes: ";
            int stunFrequency = acquiredAttackMods["stun"].Frequency;
            int turnsToStun = (stunFrequency - (GameManager.GetTurnNumber() % stunFrequency)) % stunFrequency;
            if (turnsToStun == 0)
            {
                statsDisplay.text += "now";
            }
            else
            {
                statsDisplay.text += turnsToStun;
            }
            statsDisplay.text += ", " + (turnsToStun + stunFrequency) + ", " + (turnsToStun + 2 * stunFrequency);
        }

        if (acquiredAttackMods.ContainsKey("poison"))
        {
            statsDisplay.text += "\nUpcoming " + acquiredAttackMods["poison"].DisplayName + " flourishes: ";
            int poisonFrequency = acquiredAttackMods["poison"].Frequency;
            int turnsToPoison = (poisonFrequency - (GameManager.GetTurnNumber() % poisonFrequency)) % poisonFrequency;
            if (turnsToPoison == 0)
            {
                statsDisplay.text += "now";
            }
            else
            {
                statsDisplay.text += turnsToPoison;
            }
            statsDisplay.text += ", " + (turnsToPoison + poisonFrequency) + ", " + (turnsToPoison + 2 * poisonFrequency);
        }
    }

    public bool AddAttackModifier(AttackModifier mod)
    {
        if (acquiredAttackMods.ContainsKey(mod.TypeName))
        {
            return false;
        }
        acquiredAttackMods.Add(mod.TypeName, mod);
        sortedAttackMods.Add(mod);
        sortedAttackMods.Sort(AttackModifier.SortByPrecedence);

        return true;
    }

    protected override bool Move(Vector3 direction)
    {
        bool returnValue = base.Move(direction);
        if (GameManager.SpaceContainsPickup(transform.position))
        {
            GameManager.PickupModifier(transform.position);
        }
        return returnValue;
    }

    protected override bool Attack(LivingEntity target)
    {
        // Attack a thing.
        if (target == null)
        {
            return false;
        }

        if (sortedAttackMods.Count == 0)
        {
            target.TakeDamage(damage);
        }
        else
        {
            AttackModifier.ApplyAllEffects(target, damage, sortedAttackMods);
        }

        attacking = false;

        return true;
    }

    public void GotKill()
    {
        health += healPerKill;
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CritModifier : AttackModifier {

    public static int CritMultiplier;
    
	// Use this for initialization
	public CritModifier (string typeName, string displayName, int frequency, int critMultiplier) : base(typeName, displayName, frequency)
    {
        CritMultiplier = critMultiplier;
        Debug.Log("CritModifier created!");
    }

    public override bool ApplyEffect(LivingEntity target, int damage, List<AttackModifier> modifiers)
    {
        Debug.Assert(Frequency != 0, "Frequency on crit mod is zero!");
        if (EffectProcs())
        {
            damage *= CritMultiplier;
        }
        return base.ApplyEffect(target, damage, modifiers);
    }
}

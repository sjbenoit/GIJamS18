﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackModifier {

    // Controlled by subclass
    public int Frequency;
    public string TypeName;
    public string DisplayName;

    // This determines modifier precedence.
    private static List<string> modifierPrecedence = new List<string>
    {
        "splash",
        "crit",
        "stun",
        "poison",
    };

    // Use this for initialization
    public AttackModifier (string typeName, string displayName, int frequency) {
        TypeName = typeName;
        DisplayName = displayName;
        Frequency = frequency;
	}

    public static int SortByPrecedence(AttackModifier mod1, AttackModifier mod2)
    {
        int index1 = modifierPrecedence.IndexOf(mod1.TypeName);
        Debug.Assert((index1 != -1), "Invalid TypeName '" + mod1.TypeName + "' on mod1: " + mod1);
        int index2 = modifierPrecedence.IndexOf(mod2.TypeName);
        Debug.Assert((index2 != -1), "Invalid TypeName '" + mod2.TypeName + "' on mod2: " + mod2);

        return index1.CompareTo(index2);
    }

    public bool EffectProcs()
    {
        Debug.Assert(Frequency != 0, "Frequency on a mod is zero!");
        return GameManager.GetTurnNumber() % Frequency == 0;
    }

    public static bool ApplyAllEffects(LivingEntity target, int damage, List<AttackModifier> modifiers)
    {
        // This class handles the recursion.
        if (modifiers.Count > 1)
        {
            // "Recurse" on the rest of the list of modifiers.
            return modifiers[0].ApplyEffect(target, damage, modifiers.GetRange(1, modifiers.Count - 1));
        }
        else if (modifiers.Count == 1)
        {
            return modifiers[0].ApplyEffect(target, damage, new List<AttackModifier>());
        }
        else
        {
            return target.TakeDamage(damage);
        }
    }

    // Returns true on kill
    public virtual bool ApplyEffect(LivingEntity target, int damage, List<AttackModifier> modifiers)
    {
        // This class handles the recursion.
        if (modifiers.Count > 1)
        {
            // "Recurse" on the rest of the list of modifiers.
            return modifiers[0].ApplyEffect(target, damage, modifiers.GetRange(1, modifiers.Count - 1));
        }
        else if (modifiers.Count == 1)
        {
            return modifiers[0].ApplyEffect(target, damage, new List<AttackModifier>());
        }
        else
        {
            return target.TakeDamage(damage);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemy : LivingEntity
{

    // Use this for initialization
    protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
	protected override void Update () {
        base.Update();
	}

    public void TakeTurn()
    {
        if (isStunned > 0)
        {
            Debug.Log("Enemy is stunned!");
            isStunned--;
            return;
        }
        if (poisonDuration > 0)
        {
            TakeDamage(poisonDamage);
            poisonDuration--;
        }

        // All actions an enemy can take should go below here.
        player Player = GameManager.GetPlayer();
        Vector3Int PlayerPosition = GameManager.WorldGrid.WorldToCell(Player.transform.position);
        Vector3Int position = GameManager.WorldGrid.WorldToCell(transform.position);
        int xDistance = PlayerPosition.x - position.x;
        int yDistance = PlayerPosition.y - position.y;
        
        if (xDistance == 0)
        {
            if (Mathf.Abs(yDistance) == 1)
            {
                // Attack player
                Attack(Player);
                if (yDistance > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(0f, -0.1f, -1f);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 0, 180);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(0f, 0.1f, -1f);
                }

                return;
            }
            else
            {
                if (!Move(Vector3.up * yDistance))
                {
                    if ((Random.Range(0f, 1f) < 0.5f) || !Move(Vector3.right))
                    {
                        Move(Vector3.left);
                    }
                }
            }
        }
        else if (yDistance == 0)
        {
            if (Mathf.Abs(xDistance) == 1)
            {
                // Attack player
                Attack(Player);
                if (xDistance > 0)
                {
                    transform.rotation = Quaternion.Euler(0, 0, 270);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(0.1f, 0f, -1f);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 0, 90);
                    healthDisplay.transform.rotation = Quaternion.Euler(0, 0, 0);
                    healthDisplay.transform.localPosition = new Vector3(-0.1f, 0f, -1f);
                }

                return;
            }
            else
            {
                if (!Move(Vector3.right * xDistance))
                {
                    if ((Random.Range(0f, 1f) < 0.5f) || !Move(Vector3.up))
                    {
                        Move(Vector3.down);
                    }
                }
            }
        }
        else
        {
            if ((Random.Range(0f, 1f) < 0.5f) || !Move(Vector3.up * yDistance))
            {
                Move(Vector3.right * xDistance);
            }
        }
    }

    protected override bool Attack(LivingEntity target)
    {
        // Attack a thing.
        Vector3Int targetArrayPosition = GameManager.WorldToArray(target.transform.position);
        Vector3Int myArrayPosition = GameManager.WorldToArray(transform.position);
        if (Vector3Int.Distance(targetArrayPosition, myArrayPosition) > 1f)
        {
            return false;
        }

        target.TakeDamage(damage);

        return true;
    }

    // returns true on kill
    public override bool TakeDamage(int damage)
    {
        bool isKilled = base.TakeDamage(damage);
        if (isKilled)
        {
            GameManager.RemoveEnemy(this);
        }
        return isKilled;
    }
}

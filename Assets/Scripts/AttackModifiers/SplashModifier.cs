﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashModifier : AttackModifier
{

    // Use this for initialization
    public SplashModifier(string typeName, string displayName, int frequency) : base(typeName, displayName, frequency)
    {
        Debug.Log("SplashModifier created!");
    }

    public override bool ApplyEffect(LivingEntity target, int damage, List<AttackModifier> modifiers)
    {
        Debug.Assert(Frequency != 0, "Frequency on splash mod is zero!");
        bool successful = base.ApplyEffect(target, damage, modifiers);
        if (EffectProcs())
        {
            // Splash on four adjacent squares.
            LivingEntity newTargetUp = GetAdjacentTarget(target, Vector3.up);
            if (newTargetUp != null && newTargetUp != GameManager.GetPlayer())
            {
                successful = base.ApplyEffect(newTargetUp, damage, modifiers) && successful;
            }
            LivingEntity newTargetLeft = GetAdjacentTarget(target, Vector3.left);
            if (newTargetLeft != null && newTargetLeft != GameManager.GetPlayer())
            {
                successful = base.ApplyEffect(newTargetLeft, damage, modifiers) && successful;
            }
            LivingEntity newTargetDown = GetAdjacentTarget(target, Vector3.down);
            if (newTargetDown != null && newTargetDown != GameManager.GetPlayer())
            {
                successful = base.ApplyEffect(newTargetDown, damage, modifiers) && successful;
            }
            LivingEntity newTargetRight = GetAdjacentTarget(target, Vector3.right);
            if (newTargetRight != null && newTargetRight != GameManager.GetPlayer())
            {
                successful = base.ApplyEffect(newTargetRight, damage, modifiers) && successful;
            }
        }
        return successful;
    }

    private LivingEntity GetAdjacentTarget(LivingEntity target, Vector3 direction)
    {
        return GameManager.GetEntityAt(target.transform.position + direction.normalized * GameManager.gridSize);
    }
}
